const timeMiddleware = (req, res, next)=>{
    console.log(`Time: ${new Date()}`);
    next();
}
module.exports = timeMiddleware;