const express = require('express');
const path = require('path');
const mongoose = require('mongoose');
const timeMiddleware = require('./app/middlewares/timeMiddleware');
const urlMiddleware = require('./app/middlewares/urlMiddleware');

const app = new express();
const port = 8000;

app.use(timeMiddleware);
app.use(urlMiddleware);

app.use(express.static(__dirname +"/views"));

app.get("/", (request, response) => {
   response.sendFile(path.join(__dirname + '/views/index.html'));

});

app.listen(port, () => {
    console.log(`App listening on port ${port}`)
})